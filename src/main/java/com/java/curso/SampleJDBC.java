package com.java.curso;


import com.java.curso.models.Product;
import com.java.curso.repositories.IRepository;
import com.java.curso.repositories.ProductRepositoryImpl;
import com.java.curso.util.ConnectionJDBC;

import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class SampleJDBC {

    public static void main(String[] args) {
        try (Connection conn = ConnectionJDBC.getInstance()) {
            IRepository<Product> repository = new ProductRepositoryImpl();
            System.out.println("*********** List products ***********");
            repository.list().forEach(System.out::println);

            System.out.println("*********** Product by id ***********");
            System.out.println(repository.byId(1));

            System.out.println("*********** Save product ***********");
            Product product = Product.builder()
                    .name("Producto 3")
                    .price(BigDecimal.valueOf(2300))
                    .dateCreated(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                            .parse("2022-10-02 12:10:00")).
                    build();
            repository.save(product);
            repository.list().forEach(System.out::println);

            System.out.println("*********** Edit product ***********");
            Product productEdit = Product.builder()
                    .id(3)
                    .name("Producto 3 Edited")
                    .price(BigDecimal.valueOf(2350))
                    .dateCreated(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                            .parse("2022-10-02 12:10:00")).
                    build();
            repository.save(productEdit);
            repository.list().forEach(System.out::println);

            System.out.println("*********** Delete product ***********");
            repository.deleteById(3);
            repository.list().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
