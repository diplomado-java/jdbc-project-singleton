package com.java.curso.repositories;

import com.java.curso.models.Product;
import com.java.curso.util.ConnectionJDBC;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryImpl implements IRepository<Product>  {

    private Connection getConnection() throws SQLException {
        return ConnectionJDBC.getInstance();
    }

    @Override
    public List<Product> list() {
        List<Product> products = new ArrayList<>();
        try (Statement statement = getConnection().createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM productos")) {
            while (rs.next()) {
                Product p = createProduct(rs);
                products.add(p);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public Product byId(long id) {
        Product product = new Product();
        try (PreparedStatement statement = getConnection().
                prepareStatement("SELECT * FROM productos WHERE id = ?")) {
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                product = createProduct(rs);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return product;
    }

    @Override
    public void save(Product product) {
        String sql;
        if (product.getId() != 0) sql = "UPDATE productos SET nombre = ?, precio = ? WHERE id = ?";
        else sql = "INSERT INTO productos (nombre, precio, fecha_registro) VALUE (?, ?, ?)";

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            statement.setString(1, product.getName());
            statement.setString(2, product.getPrice().toString());
            if (product.getId() != 0) statement.setLong(3, product.getId());
            else statement.setDate(3, new Date(product.getDateCreated().getTime()));
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(long id) {
        try (PreparedStatement statement = getConnection().prepareStatement("DELETE FROM productos WHERE id = ?")) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Product createProduct(ResultSet rs) throws SQLException, ParseException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setName(rs.getString("nombre"));
        product.setPrice(rs.getBigDecimal("precio"));
        product.setDateCreated(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(rs.getString("fecha_registro")));
        return product;
    }
}
 