package com.java.curso.repositories;

import java.util.List;

public interface IRepository<T> {
    List<T> list();
    T byId(long id);
    void save(T value);
    void deleteById(long id);
}
